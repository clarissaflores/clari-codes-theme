<h2 align="center">Girly Theme</h2>
<h5 align="center">Created by <a href="https://www.instagram.com/clari.codes">@clari.codes</a></h5>

A theme with girly colors because coders also love pink.

![clari.codes](https://gitlab.com/clarissaflores/clari-codes-theme/-/raw/master/images/code.png)